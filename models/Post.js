import mongoose from "mongoose";

const PostSchema = new mongoose.Schema({
	title: {
		type: String,
		required: [true, "Title is required!"],
		trim: true,
	},
	content: {
		type: String,
		required: [true, "Content is required!"],
		trim: false,
	},
	// address: {
	// 	type: String,
	// 	required: [true, "Address is required!"],
	// 	trim: true,
	// },
	// phone: {
	// 	type: String,
	// 	required: [true, "Phone is required!"],
	// 	trim: true,
	// },
	published_at: { type: Date, default: Date.now },
});

export default mongoose.models.Post ||
	mongoose.model("Post", PostSchema);
