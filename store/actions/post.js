import * as t from "../types";

export const setModalOpen = (isModalOpen) => {
	return {
		type: t.MODAL_OPEN,
		payload: isModalOpen,
	};
};

export const fetchPosts = () => {
	return {
		type: t.POST_FETCH_REQUESTED,
	};
};

export const addPost = (post) => {
	return {
		type: t.POST_ADD_REQUESTED,
		payload: post,
	};
};

export const updatePost = (post) => {
	return {
		type: t.POST_UPDATE_REQUESTED,
		payload: post,
	};
};

export const deletePost = (id) => {
	return {
		type: t.POST_DELETE_REQUESTED,
		payload: id,
	};
};

export const setSelectedPost = (id) => {
	return {
		type: t.POST_SELECTED,
		payload: id,
	};
};
