import { all, put, takeLatest } from "redux-saga/effects";
import * as t from "../types";

const api_url = "https://limitless-forest-49003.herokuapp.com";

function* fetchPosts() {
	try {
		const response = yield fetch(api_url+"/posts");

		const postList = yield response.json();
		
		yield put({
			type: t.POST_FETCH_SUCCEEDED,
			payload: postList,
		});
	} catch (error) {
		yield put({
			type: t.POST_FETCH_FAILED,
			payload: error.message,
		});
	}
}

function* watchFetchPosts() {
	yield takeLatest(t.POST_FETCH_REQUESTED, fetchPosts);
}

function* addPost(action) {
	try {
		const response = yield fetch(api_url+"/posts", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(action.payload),
		});

		const newPost = yield response.json();

		yield put({
			type: t.POST_ADD_SUCCEEDED,
			payload: newPost.data,
		});
	} catch (error) {
		yield put({
			type: t.POST_ADD_FAILED,
			payload: error.message,
		});
	}
}

function* watchAddPost() {
	yield takeLatest(t.POST_ADD_REQUESTED, addPost);
}

function* deletePost(action) {
	try {
		const response = yield fetch(api_url+"/posts/" + action.payload, {
			method: "DELETE",
		});

		const deletedPost = yield response.json();

		yield put({
			type: t.POST_DELETE_SUCCEEDED,
			payload: deletedPost.data.id,
		});
	} catch (error) {
		yield put({
			type: t.POST_DELETE_FAILED,
			payload: error.message,
		});
	}
}

function* watchRemovePost() {
	yield takeLatest(t.POST_DELETE_REQUESTED, deletePost);
}

function* updatePost(action) {
	try {
		const response = yield fetch(api_url+"/posts/" + action.payload._id, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(action.payload),
		});

		const updatedPost = yield response.json();

		yield put({
			type: t.POST_UPDATE_SUCCEEDED,
			payload: updatedPost.data,
		});
	} catch (error) {
		yield put({
			type: t.POST_UPDATE_FAILED,
			payload: error.message,
		});
	}
}

function* watchUpdatePost() {
	yield takeLatest(t.POST_UPDATE_REQUESTED, updatePost);
}

export default function* rootSaga() {
	yield all([
		watchFetchPosts(),
		watchAddPost(),
		watchRemovePost(),
		watchUpdatePost(),
	]);
}
