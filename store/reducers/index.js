import { combineReducers } from "redux";
import employeeReducer from "./employee";
import postReducer from "./post";

const rootReducer = combineReducers({
	// employee: employeeReducer,
	post: postReducer,
});

export default rootReducer;
