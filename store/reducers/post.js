import { HYDRATE } from "next-redux-wrapper";
import * as t from "../types";

const initialState = {
	postList: [],
	selectedPost: undefined,
	isModalOpen: false,
};

const mainReducer = (state = initialState, action) => {
	switch (action.type) {
		case HYDRATE:
			return { ...state, ...action.payload };
		case t.MODAL_OPEN:
			return {
				...state,
				isModalOpen: action.payload,
			};
		case t.POST_FETCH_SUCCEEDED:
			return {
				...state,
				postList: action.payload,
			};
		case t.POST_ADD_SUCCEEDED:
			return {
				...state,
				postList: [action.payload, ...state.postList],
			};
		case t.POST_UPDATE_SUCCEEDED:
			const updatedPost = state.postList.map((post) => {
				if (post._id === action.payload._id) {
					return {
						...post,
						title: action.payload.title,
						content: action.payload.content,
						published_at: action.payload.published_at,
					};
				}
				return post;
			});

			return { ...state, postList: updatedPost };
		case t.POST_DELETE_SUCCEEDED:
			const newPostList = state.postList.filter(
				(post) => post._id !== action.payload
			);
			return {
				...state,
				postList: newPostList,
			};
		case t.POST_SELECTED:
			const selectedPost = state.postList.find(
				(post) => post.id === action.payload
			);
			return {
				...state,
				selectedPost,
			};
		default:
			return state;
	}
};

export default mainReducer;
