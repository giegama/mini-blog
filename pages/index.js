// import { Header, Layout, Modal, Pagination, Table } from "@/components";
import { Header, Layout, Modal, Pagination, Table } from "@/components/posts";

function Landing() {
	return (
		<Layout>
			<Header />
			<Table />
			<Pagination />
			<Modal />
		</Layout>
	);
}

export default Landing;
