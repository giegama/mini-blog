import { useSelector, useDispatch } from "react-redux";
import { PencilSVG, TrashSVG } from "@/icons";
import {
	deletePost,
	fetchPosts,
	setModalOpen,
	setSelectedPost,
} from "@/store";
import { useEffect } from "react";

export function Table() {
	const state = useSelector((state) => state.post);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchPosts());
	}, [dispatch]);

	return (
		<table className="table">
			<thead className="table__head">
				<tr>
					<th>Title</th>
					<th>Content</th>
					<th>Published</th>
					<th>Actions</th>
				</tr>
			</thead>

			<tbody className="table__body">
				{state.postList.map(({ id, title, content, published_at }) => (
					<tr key={id}>
						<td>{title}</td>
						<td>{content}</td>
						<td>{published_at}</td>
						<td>
							<button
								className="btn btn__compact btn__edit"
								onClick={() => {
									dispatch(setSelectedPost(id));
									dispatch(setModalOpen(true));
								}}
							>
								<PencilSVG />
							</button>
							<button
								className="btn btn__compact btn__delete"
								onClick={() => {
									dispatch(deletePost(id));
								}}
							>
								<TrashSVG />
							</button>
						</td>
					</tr>
				))}
			</tbody>
		</table>
	);
}
