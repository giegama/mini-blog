import { useEffect } from "react";
import ReactDOM from "react-dom";

import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import cx from "clsx";

import { CheckSVG, CloseSVG } from "@/icons";
import {
	addPost,
	setModalOpen,
	setSelectedPost,
	updatePost,
} from "@/store";

export function Modal() {
	const { register, handleSubmit, errors, reset, setValue } = useForm();

	const state = useSelector((state) => state.post);

	const dispatch = useDispatch();

	const closeModal = () => {
		reset();
		dispatch(setModalOpen(false));
		dispatch(setSelectedPost(undefined));
	};

	const onSubmitHandler = (data) => {
		if (data) {
			closeModal();
		}
		if (state.selectedPost !== undefined) {
			dispatch(
				updatePost({
					_id: state.selectedPost.id,
					...data,
				})
			);
		} else {
			dispatch(addPost(data));
		}
	};

	useEffect(() => {
		if (state.selectedPost !== undefined) {
			setValue("title", state.selectedPost.title);
			setValue("content", state.selectedPost.content);
		}
	}, [state.selectedPost, setValue]);

	return state.isModalOpen
		? ReactDOM.createPortal(
				<div className="modal">
					<div className="modal__content">
						<header className="header modal__header">
							<h1 className="header__h2">
								{state.selectedPost !== undefined ? (
									<>
										Edit <span>Post</span>
									</>
								) : (
									<>
										Add <span>Post</span>
									</>
								)}
							</h1>
							<button
								className="btn btn__compact btn__close"
								onClick={closeModal}
							>
								<CloseSVG />
							</button>
						</header>

						<form
							className="form modal__form"
							onSubmit={handleSubmit(onSubmitHandler)}
							noValidate
						>
							<div className="form__element">
								<label
									htmlFor="titleInput"
									className={cx("label", errors.name && "label--error")}
								>
									{errors.title ? (
										"Title is required!"
									) : (
										<>
											Title&nbsp;<span className="label__required">*</span>
										</>
									)}
								</label>
								<input
									type="text"
									id="titleInput"
									name="title"
									placeholder="Title"
									className={cx("input", errors.title && "input--error")}
									ref={register({ required: true })}
								/>
							</div>

							<div className="form__element">
								<label
									htmlFor="contentPost"
									className={cx("label", errors.address && "label--error")}
								>
									{errors.content ? (
										"Content is required!"
									) : (
										<>
											Content&nbsp;<span className="label__required">*</span>
										</>
									)}
								</label>
								<textarea
									type="text"
									id="contentPost"
									name="content"
									placeholder="Content"
									className={cx("area", errors.content && "input--error")}
									ref={register({ required: true })}
								/>
							</div>

							<div className="form__action">
								<button
									className="btn btn__icon btn__cancel"
									type="button"
									onClick={closeModal}
								>
									<CloseSVG /> Cancel
								</button>
								<button className="btn btn__primary btn__icon" type="submit">
									<CheckSVG /> {state.selectedPost !== undefined ? "Update" : "Submit"}
								</button>
							</div>
						</form>
					</div>
				</div>,
				document.body
		  )
		: null;
}
